package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.endpoint.Task;
import ru.ekfedorov.tm.endpoint.TaskEndpoint;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class TaskListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show all task.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-list";
    }

    @Override
    @SneakyThrows
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        if (isEmpty(sort)) tasks = taskEndpoint.findAllTask(session);
        else tasks = taskEndpoint.findTaskAllWithComparator(session, sort);
        if (tasks.isEmpty()) {
            System.out.println("--- There are no tasks ---");
            return;
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + toString(task));
            index++;
        }
        System.out.println();
    }

    @NotNull
    public String toString(@NotNull final Task task) {
        return task.getId() + ": " + task.getName() + " | " + task.getStatus();
    }

}
