package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.endpoint.Status;
import ru.ekfedorov.tm.endpoint.Task;
import ru.ekfedorov.tm.endpoint.TaskEndpoint;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Change task status by index.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "change-task-status-by-index";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (bootstrap == null) throw new NullObjectException();
        @Nullable final Session session = bootstrap.getSession();
        if (endpointLocator == null) throw new NullObjectException();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        @NotNull final Task task = taskEndpoint.findTaskOneByIndex(session, index);
        if (task == null) throw new NullTaskException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Task taskUpdated = taskEndpoint.changeTaskStatusByIndex(session, index, status);
        if (taskUpdated == null) throw new NullTaskException();
    }

}
