package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.marker.IntegrationCategory;

public class UserEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        sessionAdmin = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.getSessionEndpoint().closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserTest() {
        endpointLocator.getAdminUserEndpoint().removeByLogin(sessionAdmin, "test2");
        endpointLocator.getUserEndpoint().createUser("test2", "test2", "test@twst.ru");
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin(session, "test2"));
        endpointLocator.getAdminUserEndpoint().removeByLogin(sessionAdmin, "test2");

    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLoginTest() {
        endpointLocator.getAdminUserEndpoint().removeByLogin(sessionAdmin, "test2");
        endpointLocator.getUserEndpoint().createUser("test2", "test2", "test@twst.ru");
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin(session, "test2"));
        endpointLocator.getAdminUserEndpoint().removeByLogin(sessionAdmin, "test2");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserOneBySessionTest() {
        final String login = endpointLocator.getUserEndpoint().findUserOneBySession(session).getLogin();
        Assert.assertEquals("test", login);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void setPasswordTest() {
        final String passwordNew = "newTestPass";
        endpointLocator.getUserEndpoint().setPassword(session, passwordNew);
        final Session session2 = endpointLocator.getSessionEndpoint().openSession("test", passwordNew);
        Assert.assertNotNull(session2);
        endpointLocator.getUserEndpoint().setPassword(session2, "test");
        endpointLocator.getSessionEndpoint().closeSession(session2);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateUpdateTest() {
        final String firstName = "fName";
        final String lastName = "lName";
        final String middleName = "mName";
        endpointLocator.getUserEndpoint().updateUser(session, firstName, lastName, middleName);
        final User user = endpointLocator.getUserEndpoint().findUserOneBySession(session);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }


}
