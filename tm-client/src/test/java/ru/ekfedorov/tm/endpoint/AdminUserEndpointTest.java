package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.marker.IntegrationCategory;

public class AdminUserEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getSessionEndpoint().closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithRole() {
        final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        adminUserEndpoint.createUserWithRole(sessionAdmin, "test2", "test2", Role.ADMIN);
        final User user = endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "test2");
        Assert.assertEquals(Role.ADMIN, user.getRole());
        adminUserEndpoint.removeByLogin(sessionAdmin, "test2");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllUser() {
        Assert.assertFalse(endpointLocator.getAdminUserEndpoint().findAllUser(sessionAdmin).isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserOneById() {
        final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        Assert.assertNotNull(adminUserEndpoint.findUserOneById(sessionAdmin, sessionAdmin.getUserId()));
    }

    @Test(expected = UserIsLockedException_Exception.class)
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void lockUserByLogin() {
        final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        adminUserEndpoint.lockUserByLogin(sessionAdmin, "test");
        endpointLocator.getSessionEndpoint().openSession("test", "test");
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeByLogin() {
        final User user = endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "test");
        endpointLocator.getAdminUserEndpoint().removeByLogin(sessionAdmin, "test");
        Assert.assertNull(endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "test"));
        endpointLocator.getUserEndpoint().createUser(user.getLogin(), user.getLogin(), user.getEmail());
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "test"));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserOneById() {
        final User user = endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "test");
        endpointLocator.getAdminUserEndpoint().removeUserOneById(sessionAdmin, user.getId());
        Assert.assertNull(endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "test"));
        endpointLocator.getUserEndpoint().createUser(user.getLogin(), user.getLogin(), user.getEmail());
        Assert.assertNotNull(endpointLocator.getUserEndpoint().findUserByLogin(sessionAdmin, "test"));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByLogin() {
        final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        adminUserEndpoint.unlockUserByLogin(sessionAdmin, "test");
        Assert.assertNotNull(endpointLocator.getSessionEndpoint().openSession("test", "test"));
    }

}
