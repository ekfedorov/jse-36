package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.ISessionRepository;
import ru.ekfedorov.tm.marker.UnitCategory;
import ru.ekfedorov.tm.model.Session;

import java.util.ArrayList;
import java.util.List;

public class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionRepository.addAll(sessions);
        Assert.assertTrue(sessionRepository.findOneById(session1.getId()).isPresent());
        Assert.assertTrue(sessionRepository.findOneById(session2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Session session = new Session();
        Assert.assertNotNull(sessionRepository.add(session));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        sessionRepository.clear();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() {
        final Session session1 = new Session();
        final String sessionId = session1.getId();
        sessionRepository.add(session1);
        Assert.assertTrue(sessionRepository.contains(sessionId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        final List<Session> sessions = new ArrayList<>();
        final Session session1 = new Session();
        final Session session2 = new Session();
        sessions.add(session1);
        sessions.add(session2);
        sessionRepository.addAll(sessions);
        Assert.assertEquals(2, sessionRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final Session session1 = new Session();
        final String sessionId = session1.getId();
        sessionRepository.add(session1);
        Assert.assertNotNull(sessionRepository.findOneById(sessionId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final Session session = new Session();
        sessionRepository.add(session);
        final String sessionId = session.getId();
        Assert.assertTrue(sessionRepository.findOneById(sessionId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        final Session session = new Session();
        sessionRepository.add(session);
        final String sessionId = session.getId();
        Assert.assertTrue(sessionRepository.removeOneById(sessionId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Session session = new Session();
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.remove(session));
    }

}
