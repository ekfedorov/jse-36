package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.marker.UnitCategory;
import ru.ekfedorov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.addAll(users);
        Assert.assertTrue(userRepository.findOneById(user1.getId()).isPresent());
        Assert.assertTrue(userRepository.findOneById(user2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(userRepository.add(user));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() {
        final User user1 = new User();
        final String userId = user1.getId();
        userRepository.add(user1);
        Assert.assertTrue(userRepository.contains(userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.addAll(users);
        Assert.assertEquals(2, userRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByLogin() {
        final User user = new User();
        user.setLogin("test");
        userRepository.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userRepository.findByLogin(login).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final User user1 = new User();
        final String userId = user1.getId();
        userRepository.add(user1);
        Assert.assertNotNull(userRepository.findOneById(userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final User user = new User();
        userRepository.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userRepository.findOneById(userId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void isLoginExist() {
        final User user = new User();
        user.setLogin("test");
        userRepository.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userRepository.isLoginExist(login));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLogin() {
        final User user = new User();
        user.setLogin("test");
        userRepository.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userRepository.removeByLogin(login);
        Assert.assertFalse(userRepository.isLoginExist(login));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        final User user = new User();
        userRepository.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userRepository.removeOneById(userId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final User user = new User();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.remove(user));
    }

}
