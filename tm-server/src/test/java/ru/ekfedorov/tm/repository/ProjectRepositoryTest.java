package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.marker.UnitCategory;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectRepository.addAll(projects);
        Assert.assertTrue(projectRepository.findOneById(project1.getId()).isPresent());
        Assert.assertTrue(projectRepository.findOneById(project2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Project project = new Project();
        Assert.assertNotNull(projectRepository.add(project));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() {
        final Project project1 = new Project();
        final String projectId = project1.getId();
        projectRepository.add(project1);
        Assert.assertTrue(projectRepository.contains(projectId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectRepository.addAll(projects);
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllSortByUserId() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final User user = new User();
        final String userId = user.getId();
        project1.setName("b");
        project2.setName("c");
        project3.setName("a");
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projectRepository.addAll(projects);
        final String sort = "NAME";
        final Sort sortType = Sort.valueOf(sort);
        final Comparator<Project> comparator = sortType.getComparator();
        final List<Project> projects2 = new ArrayList<>(projectRepository.findAll(userId, comparator));
        Assert.assertFalse(projects2.isEmpty());
        Assert.assertEquals(3, projects2.size());
        Assert.assertEquals(0, projects2.indexOf(project3));
        Assert.assertEquals(1, projects2.indexOf(project1));
        Assert.assertEquals(2, projects2.indexOf(project2));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final Project project1 = new Project();
        final String projectId = project1.getId();
        projectRepository.add(project1);
        Assert.assertNotNull(projectRepository.findOneById(projectId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final Project project = new Project();
        projectRepository.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectRepository.findOneById(projectId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTestByUserId() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectRepository.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectRepository.findOneById(userId, projectId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectRepository.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectRepository.findOneByName(userId, name).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        final Project project1 = new Project();
        projectRepository.add(project1);
        final String projectId = project1.getId();
        Assert.assertTrue(projectRepository.removeOneById(projectId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTestByUserId() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectRepository.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectRepository.removeOneById(userId, projectId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIndexTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final User user = new User();
        final String userId = user.getId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projectRepository.add(project1);
        projectRepository.add(project2);
        projectRepository.add(project3);
        Assert.assertTrue(projectRepository.findOneByIndex(userId, 0).isPresent());
        Assert.assertTrue(projectRepository.findOneByIndex(userId, 1).isPresent());
        Assert.assertTrue(projectRepository.findOneByIndex(userId, 2).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectRepository.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectRepository.removeOneByName(userId, name));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Project project = new Project();
        projectRepository.add(project);
        Assert.assertNotNull(projectRepository.remove(project));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTestByUserIdAndObject() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectRepository.add(project);
        Assert.assertTrue(projectRepository.remove(userId, project));
    }

}
