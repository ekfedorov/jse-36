package ru.ekfedorov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.marker.UnitCategory;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertTrue(taskService.findOneById(task1.getId()).isPresent());
        Assert.assertTrue(taskService.findOneById(task2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Task task = new Task();
        Assert.assertNotNull(taskService.add(task));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        taskService.clear();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() {
        final Task task1 = new Task();
        final String taskId = task1.getId();
        taskService.add(task1);
        Assert.assertTrue(taskService.contains(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        tasks.add(task1);
        tasks.add(task2);
        taskService.addAll(tasks);
        Assert.assertEquals(2, taskService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllSortByUserId() {
        final List<Task> tasks = new ArrayList<>();
        final Task task1 = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        final User user = new User();
        final String userId = user.getId();
        task1.setName("b");
        task2.setName("c");
        task3.setName("a");
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);
        taskService.addAll(tasks);
        final String sort = "NAME";
        final Sort sortType = Sort.valueOf(sort);
        final Comparator<Task> comparator = sortType.getComparator();
        final List<Task> tasks2 = new ArrayList<>(taskService.findAll(userId, comparator));
        Assert.assertFalse(tasks2.isEmpty());
        Assert.assertEquals(3, tasks2.size());
        Assert.assertEquals(0, tasks2.indexOf(task3));
        Assert.assertEquals(1, tasks2.indexOf(task1));
        Assert.assertEquals(2, tasks2.indexOf(task2));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final Task task1 = new Task();
        final String taskId = task1.getId();
        taskService.add(task1);
        Assert.assertNotNull(taskService.findOneById(taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final Task task = new Task();
        taskService.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskService.findOneById(taskId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTestByUserId() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskService.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskService.findOneById(userId, taskId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("pr1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(taskService.findOneByName(userId, name).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        final Task task1 = new Task();
        taskService.add(task1);
        final String taskId = task1.getId();
        Assert.assertTrue(taskService.removeOneById(taskId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTestByUserId() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskService.add(task);
        final String taskId = task.getId();
        Assert.assertTrue(taskService.removeOneById(userId, taskId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIndexTest() {
        final Task task1 = new Task();
        final Task task2 = new Task();
        final Task task3 = new Task();
        final User user = new User();
        final String userId = user.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        taskService.add(task1);
        taskService.add(task2);
        taskService.add(task3);
        Assert.assertTrue(taskService.findOneByIndex(userId, 0).isPresent());
        Assert.assertTrue(taskService.findOneByIndex(userId, 1).isPresent());
        Assert.assertTrue(taskService.findOneByIndex(userId, 2).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        task.setName("pr1");
        taskService.add(task);
        final String name = task.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(taskService.removeOneByName(userId, name));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Task task = new Task();
        taskService.add(task);
        Assert.assertNotNull(taskService.remove(task));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTestByUserIdAndObject() {
        final Task task = new Task();
        final User user = new User();
        final String userId = user.getId();
        task.setUserId(userId);
        taskService.add(task);
        Assert.assertTrue(taskService.remove(userId, task));
    }

}
