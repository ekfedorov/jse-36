package ru.ekfedorov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.marker.UnitCategory;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertTrue(projectService.findOneById(project1.getId()).isPresent());
        Assert.assertTrue(projectService.findOneById(project2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final Project project = new Project();
        Assert.assertNotNull(projectService.add(project));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        projectService.clear();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() {
        final Project project1 = new Project();
        final String projectId = project1.getId();
        projectService.add(project1);
        Assert.assertTrue(projectService.contains(projectId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertEquals(2, projectService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllSortByUserId() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final User user = new User();
        final String userId = user.getId();
        project1.setName("b");
        project2.setName("c");
        project3.setName("a");
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projects.add(project1);
        projects.add(project2);
        projects.add(project3);
        projectService.addAll(projects);
        final String sort = "NAME";
        final Sort sortType = Sort.valueOf(sort);
        final Comparator<Project> comparator = sortType.getComparator();
        final List<Project> projects2 = new ArrayList<>(projectService.findAll(userId, comparator));
        Assert.assertFalse(projects2.isEmpty());
        Assert.assertEquals(3, projects2.size());
        Assert.assertEquals(0, projects2.indexOf(project3));
        Assert.assertEquals(1, projects2.indexOf(project1));
        Assert.assertEquals(2, projects2.indexOf(project2));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final Project project1 = new Project();
        final String projectId = project1.getId();
        projectService.add(project1);
        Assert.assertNotNull(projectService.findOneById(projectId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final Project project = new Project();
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(projectId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTestByUserId() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(userId, projectId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByNameTest() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectService.findOneByName(userId, name).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        final Project project1 = new Project();
        projectService.add(project1);
        final String projectId = project1.getId();
        Assert.assertTrue(projectService.removeOneById(projectId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTestByUserId() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.removeOneById(userId, projectId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIndexTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final User user = new User();
        final String userId = user.getId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projectService.add(project1);
        projectService.add(project2);
        projectService.add(project3);
        Assert.assertTrue(projectService.findOneByIndex(userId, 0).isPresent());
        Assert.assertTrue(projectService.findOneByIndex(userId, 1).isPresent());
        Assert.assertTrue(projectService.findOneByIndex(userId, 2).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByNameTest() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectService.removeOneByName(userId, name));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final Project project = new Project();
        projectService.add(project);
        Assert.assertNotNull(projectService.remove(project));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTestByUserIdAndObject() {
        final Project project = new Project();
        final User user = new User();
        final String userId = user.getId();
        project.setUserId(userId);
        projectService.add(project);
        Assert.assertTrue(projectService.remove(userId, project));
    }

}
