package ru.ekfedorov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.IUserService;
import ru.ekfedorov.tm.marker.UnitCategory;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @Test
    @Category(UnitCategory.class)
    public void addAllTest() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertTrue(userService.findOneById(user1.getId()).isPresent());
        Assert.assertTrue(userService.findOneById(user2.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(userService.add(user));
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() {
        userService.clear();
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void containsTest() {
        final User user1 = new User();
        final String userId = user1.getId();
        userService.add(user1);
        Assert.assertTrue(userService.contains(userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userService.addAll(users);
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByLogin() {
        final User user = new User();
        user.setLogin("test");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.findByLogin(login).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIdTest() {
        final User user1 = new User();
        final String userId = user1.getId();
        userService.add(user1);
        Assert.assertNotNull(userService.findOneById(userId));
    }

    @Test
    @Category(UnitCategory.class)
    public void findOneByIndexTest() {
        final User user = new User();
        userService.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userService.findOneById(userId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void isLoginExist() {
        final User user = new User();
        user.setLogin("test");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userService.isLoginExist(login));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLogin() {
        final User user = new User();
        user.setLogin("test");
        userService.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userService.removeByLogin(login);
        Assert.assertFalse(userService.isLoginExist(login));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeOneByIdTest() {
        final User user = new User();
        userService.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userService.removeOneById(userId).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        final User user = new User();
        userService.add(user);
        Assert.assertNotNull(userService.remove(user));
    }

}
