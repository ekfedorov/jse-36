package ru.ekfedorov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IBusinessRepository<Task> {

    @NotNull
    Optional<Task> bindTaskByProjectId(
            @NotNull String userId, @NotNull String projectId, @NotNull String taskId
    );

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Optional<Task> unbindTaskFromProjectId(@NotNull String userId, @NotNull String taskId);

}