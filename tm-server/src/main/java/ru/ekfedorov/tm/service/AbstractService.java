package ru.ekfedorov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;


public class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@Nullable final E entity) {
        if (entity == null) throw new NullObjectException();
        return repository.add(entity);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<E> entities) {
        if (entities == null) throw new NullObjectException();
        repository.addAll(entities);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(
            @Nullable final Comparator<E> comparator
    ) {
        if (comparator == null) throw new NullComparatorException();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<E> findOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public E remove(@Nullable final E entity) {
        if (entity == null) throw new NullObjectException();
        return repository.remove(entity);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<E> removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return repository.removeOneById(id);
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return repository.contains(id);
    }

}
